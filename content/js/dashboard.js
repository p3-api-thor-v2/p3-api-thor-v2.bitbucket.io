/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9824619371558148, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9419354838709677, 500, 1500, "getDashboardData"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.013059701492537313, 500, 1500, "getChildCheckInCheckOutByArea"], "isController": false}, {"data": [0.9934086629001884, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.9979508196721312, 500, 1500, "findAllConfigByCategory (health_check_type)"], "isController": false}, {"data": [0.997982091058141, 500, 1500, "getAllEvents"], "isController": false}, {"data": [0.9975847612103537, 500, 1500, "addOrUpdateUserDevice"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.9938524590163934, 500, 1500, "findAllConfigByCategory (Relation_Child)"], "isController": false}, {"data": [0.9792663476874003, 500, 1500, "me"], "isController": false}, {"data": [0.9984413965087282, 500, 1500, "getNotificationCount"], "isController": false}, {"data": [0.9934086629001884, 500, 1500, "dismissChildCheckInByVPS"], "isController": false}, {"data": [0.995291902071563, 500, 1500, "addClassesToArea"], "isController": false}, {"data": [0.9959016393442623, 500, 1500, "findAllConfigByCategory (non_parent_relationship)"], "isController": false}, {"data": [0.9959016393442623, 500, 1500, "findAllConfigByCategory (guardian_rejection_reason)"], "isController": false}, {"data": [0.3975409836065574, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9978854463546933, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.984375, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.9981775104793147, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}, {"data": [0.9973528299508383, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.9997266265718973, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9959016393442623, 500, 1500, "findAllConfigByCategory (checkout_decline_reason)"], "isController": false}, {"data": [1.0, 500, 1500, "getAllCentreClasses"], "isController": false}, {"data": [0.9811676082862524, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.9979508196721312, 500, 1500, "findAllConfigByCategory (checkin_decline_reason)"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 77175, 0, 0.0, 97.27051506316775, 2, 18044, 17.0, 104.0, 289.0, 2076.9100000000144, 253.20878774754914, 1155.626112838063, 262.6523202705651], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getDashboardData", 2945, 0, 0.0, 355.75382003395583, 90, 2103, 330.0, 513.4000000000001, 601.7999999999993, 1185.54, 9.840086071516877, 85.41848153292335, 26.195385381791997], "isController": false}, {"data": ["findAllConfigByCategory", 627, 0, 0.0, 7.266347687400317, 2, 216, 4.0, 12.0, 20.0, 63.72000000000003, 2.1000030143583936, 1.9423477529214155, 1.8589524664987556], "isController": false}, {"data": ["getChildCheckInCheckOutByArea", 536, 0, 0.0, 2463.3376865671635, 1077, 4542, 2461.0, 3148.9, 3415.6499999999996, 4137.41, 1.7803641774783932, 9.589724251150926, 5.911365433033727], "isController": false}, {"data": ["getAllClassInfo", 531, 0, 0.0, 52.467043314500934, 9, 1521, 28.0, 80.80000000000001, 140.79999999999973, 626.8399999999981, 1.7863751051303616, 1.9730373475609757, 1.7462514455424727], "isController": false}, {"data": ["findAllLevels", 244, 0, 0.0, 21.999999999999993, 5, 187, 11.0, 47.5, 81.5, 181.85000000000008, 0.8344676165005712, 0.531321177693723, 0.7024522318588793], "isController": false}, {"data": ["findAllConfigByCategory (health_check_type)", 244, 0, 0.0, 32.15163934426229, 5, 712, 12.0, 63.0, 162.0, 418.15000000000174, 0.8348987685243164, 0.8226688060947611, 0.8022855353788354], "isController": false}, {"data": ["getAllEvents", 7929, 0, 0.0, 23.93189557321215, 6, 1605, 11.0, 38.0, 67.5, 201.69999999999982, 26.518926801206714, 13.129976453331839, 28.797897073185414], "isController": false}, {"data": ["addOrUpdateUserDevice", 10972, 0, 0.0, 34.70060153117042, 9, 2138, 19.0, 61.0, 96.0, 263.27000000000044, 36.690252939366786, 17.700180617233585, 45.160653485565675], "isController": false}, {"data": ["getHomefeed", 96, 0, 0.0, 15193.885416666664, 5260, 18044, 15349.0, 17060.5, 17351.35, 18044.0, 0.3159734450650543, 59.567474318929115, 0.9599544800755704], "isController": false}, {"data": ["findAllConfigByCategory (Relation_Child)", 244, 0, 0.0, 40.827868852459034, 5, 1170, 13.0, 85.5, 137.0, 612.9000000000009, 0.8349387654540663, 0.6669725684974865, 0.7998778602641007], "isController": false}, {"data": ["me", 627, 0, 0.0, 98.29186602870817, 28, 2216, 49.0, 150.4000000000001, 357.2000000000006, 1038.8400000000013, 2.099510783850844, 3.512032554346188, 4.806333236436055], "isController": false}, {"data": ["getNotificationCount", 8020, 0, 0.0, 26.25910224438913, 7, 1727, 14.0, 43.0, 72.0, 222.0, 26.82292189245413, 13.93534613943906, 28.90835729879464], "isController": false}, {"data": ["dismissChildCheckInByVPS", 531, 0, 0.0, 39.42937853107344, 6, 1577, 15.0, 61.80000000000001, 100.79999999999984, 655.879999999998, 1.7865313702796544, 0.9927112789932846, 1.364323761287783], "isController": false}, {"data": ["addClassesToArea", 531, 0, 0.0, 77.14689265536727, 14, 868, 58.0, 141.0, 193.5999999999998, 500.51999999999805, 1.7858043686626646, 0.9783557136911667, 1.2992424361852393], "isController": false}, {"data": ["findAllConfigByCategory (non_parent_relationship)", 244, 0, 0.0, 42.38524590163935, 5, 704, 13.0, 112.5, 171.0, 525.9500000000032, 0.8349473367233108, 0.6824716023802844, 0.8072244759336696], "isController": false}, {"data": ["findAllConfigByCategory (guardian_rejection_reason)", 244, 0, 0.0, 32.553278688524586, 5, 525, 11.0, 60.5, 151.5, 466.65000000000094, 0.8348187861597993, 0.7948714028376996, 0.8087306990923057], "isController": false}, {"data": ["getCountCheckInOutChildren", 244, 0, 0.0, 1186.8073770491808, 365, 2285, 1137.0, 1697.5, 1779.75, 2137.650000000001, 0.8309070167373279, 0.5241854812620252, 0.9688505644378609], "isController": false}, {"data": ["getCentreHolidaysOfYear", 10877, 0, 0.0, 34.996138641169466, 10, 1714, 19.0, 61.0, 97.0, 258.0999999999967, 36.37318209330555, 65.84482918615967, 36.0832527017797], "isController": false}, {"data": ["findAllChildrenByParent", 96, 0, 0.0, 91.5625, 20, 1798, 31.0, 177.2, 297.7499999999991, 1798.0, 0.32147234333246266, 0.6906632376283378, 0.4925684635631191], "isController": false}, {"data": ["findAllSchoolConfig", 10974, 0, 0.0, 37.84308365226919, 8, 1707, 22.0, 66.0, 106.0, 265.25, 36.69448679881229, 800.2551554600352, 21.967150791720165], "isController": false}, {"data": ["getChildCheckInCheckOut", 249, 0, 0.0, 4598.140562248997, 3093, 6157, 4640.0, 5203.0, 5381.5, 5911.0, 0.8209070858457816, 55.97159358072913, 2.4122162317480047], "isController": false}, {"data": ["getClassAttendanceSummaries", 7933, 0, 0.0, 27.46716248581873, 6, 1766, 12.0, 48.0, 87.0, 252.65999999999985, 26.52955438508486, 15.026505413426973, 29.09442341254912], "isController": false}, {"data": ["getLatestMobileVersion", 10974, 0, 0.0, 15.284763987607166, 8, 797, 12.0, 21.0, 27.0, 64.0, 36.60013007153935, 19.88992383310821, 21.48550843588974], "isController": false}, {"data": ["findAllConfigByCategory (checkout_decline_reason)", 244, 0, 0.0, 32.270491803278695, 5, 1352, 11.0, 47.0, 101.0, 501.00000000000114, 0.8346445919135252, 0.5762634047684204, 0.8069317831976467], "isController": false}, {"data": ["getAllCentreClasses", 244, 0, 0.0, 42.06557377049182, 13, 326, 25.0, 82.5, 146.25, 267.0500000000001, 0.8343820101767249, 1.144830785447557, 0.9468280232669475], "isController": false}, {"data": ["getAllArea", 531, 0, 0.0, 82.92467043314494, 10, 1532, 32.0, 136.60000000000002, 298.39999999999975, 1184.3599999999924, 1.7856902651296054, 3.3115486459776573, 1.7560450165874149], "isController": false}, {"data": ["findAllConfigByCategory (checkin_decline_reason)", 244, 0, 0.0, 60.02049180327866, 5, 1076, 17.0, 185.5, 261.25, 455.3000000000003, 0.8350216284290642, 0.5536910993196626, 0.8064808501136176], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 77175, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
